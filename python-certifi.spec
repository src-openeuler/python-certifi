%global _empty_manifest_terminate_build 0
Name:           python-certifi
Version:        2025.1.31
Release:        1
Summary:        Python package for providing Mozilla's CA Bundle.
License:        MPL-2.0
URL:            https://github.com/certifi/python-certifi
Source0:        https://files.pythonhosted.org/packages/1c/ab/c9f1e32b7b1bf505bf26f0ef697775960db7932abeb7b516de930ba2705f/certifi-2025.1.31.tar.gz
BuildArch:      noarch

%description
Certifi provides Mozilla carefully curated collection of Root Certificates for validating the
trustworthiness of SSL certificates while verifying the identity of TLS hosts. It has been
extracted from the Requests project.

%package -n python3-certifi
Summary:        Python package for providing Mozilla's CA Bundle.
Provides:       python-certifi = %{version}-%{release}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%description -n python3-certifi
Certifi provides Mozilla carefully curated collection of Root Certificates for validating the
trustworthiness of SSL certificates while verifying the identity of TLS hosts. It has been
extracted from the Requests project.

%package help
Summary:        Python package for providing Mozilla's CA Bundle.
Provides:       python3-certifi-doc
%description help
Certifi provides Mozilla carefully curated collection of Root Certificates for validating the
trustworthiness of SSL certificates while verifying the identity of TLS hosts. It has been
extracted from the Requests project.

%prep
%autosetup -n certifi-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-certifi -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sat Feb 01 2025 Funda Wang <fundawang@yeah.net> - 2025.1.31-1
- update to 2025.1.31

* Mon Dec 16 2024 liuzhilin <liuzhilin@kylinos.cn> - 2024.12.14-1
- Update package version to 2024.12.14
- Bump pypa/gh-action-pypi-publish from 1.12.2 to 1.12.3 
- Bump actions/upload-artifact from 4.4.1 to 4.4.3 
- Bump actions/checkout from 4.1.7 to 4.2.0

* Fri Aug 30 2024 guochao <guochao@kylinos.cn> - 2024.8.30-1
- Update package to version 2024.8.30
- Bump actions/upload-artifact from 4.3.5 to 4.3.6
- Bump actions/setup-python from 5.1.0 to 5.1.1
- Bump actions/download-artifact from 4.1.7 to 4.1.8 

* Sat Jul 06 2024 yanjianqing <yanjianqing@kylinos.cn> - 2024.7.4-1
- Update to 2024.7.4 to fix CVE-2024-39689

* Tue Jun 04 2024 liuzhilin <liuzhilin@kylinos.cn> - 2024.06.02-1
- Update to 2024.06.02

* Fri Mar 8 2024 xieyanlong <xieyanlong@kylinos.cn> - 2024.2.2-1
- Update to 2024.2.2 for fix CVE-2023-40104

* Mon Jul 31 2023 yaoxin <yao_xin001@hoperun.com> - 2023.7.22-1
- Update to 2023.7.22 for fix CVE-2023-37920 and CVE-2022-23491

* Thu May 18 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2023.5.7-1
- Update package to version 2023.5.7

* Thu Dec 15 2022 liqiuyu <liqiuyu@kylinos.cn> - 2022.12.7-1
- Update package to version 2022.12.7

* Mon Sep 26 2022 liqiuyu <liqiuyu@kylinos.cn> - 2022.9.24-1
- Upgrade package to version 2022.9.24

* Wed Jan 19 2022 SimpleUpdate Robot <tc@openeuler.org> - 2021.10.8-1
- Upgrade to version 2021.10.8

* Wed Jul 14 2021 OpenStack_SIG <openstack@openeuler.org> - 2020.12.5-1
- Upgrade to version 2020.12.5

* Tue Aug 4 2020 wangxiao <wangxiao65@huawei.com> - 2020.6.20-1
- package init
